package com.example.icc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.icc.model.CoachingStaff;
import com.example.icc.model.Country;

public interface CoachingStaffRepo extends JpaRepository<CoachingStaff, Long>{
	List<CoachingStaff> getAllCoachingStaffByCountry(Country country);

}
