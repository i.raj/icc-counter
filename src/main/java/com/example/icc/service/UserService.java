package com.example.icc.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.icc.model.User;
import com.example.icc.repo.UserRepo;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

	UserRepo repo;

	/*************************************************************************
	 * Create a new User
	 * 
	 * @param ob {@link User} object
	 * @return {@link User}
	 *************************************************************************/
	public User saveUser(User ob, HttpServletResponse rs) {
		try {
			return repo.save(ob);
		} catch (Exception e) {
			rs.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return ob;
		}
	}

	/*************************************************************************
	 * Get all {@link User}
	 * 
	 * @return {@link User}
	 *************************************************************************/
	public List<User> getAll() {
		return repo.findAll();
	}

	/*************************************************************************
	 * Update {@link User}
	 * 
	 * @param ob {@link User} object
	 * @return {@link User}
	 *************************************************************************/

	public User update(User ob, HttpServletResponse rs) {
		try {
			return repo.save(ob);
		} catch (Exception e) {
			rs.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return ob;
		}
	}

	/*************************************************************************
	 * Delete {@link User}
	 * 
	 * @param id Id of User
	 * @return
	 *************************************************************************/

	public String delete(Long id) {
		repo.deleteById(id);
		return "Deleted Sucessfully!!!!!!!";
	}
}
