package com.example.icc.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.icc.model.CoachingStaff;
import com.example.icc.repo.CoachingStaffRepo;
import com.example.icc.repo.CountryRepo;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CoachingStaffService {

	CoachingStaffRepo repo;
	CountryRepo countryRepo;

	/*************************************************************************
	 * Create a new CoachingStaff
	 * 
	 * @param ob {@link CoachingStaff} object
	 * @return {@link CoachingStaff}
	 *************************************************************************/
	public CoachingStaff saveCoachingStaff(CoachingStaff ob, HttpServletResponse rs) {
		try {
			ob.setCountry(countryRepo.findById(ob.getCountryId()).orElse(null));
			return repo.save(ob);
		} catch (Exception e) {
			rs.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return ob;
		}
	}

	/*************************************************************************
	 * Get all {@link CoachingStaff} by Country
	 * 
	 * @return {@link CoachingStaff}
	 *************************************************************************/
	public List<CoachingStaff> getAllByCountry(Long countryId) {
		return repo.getAllCoachingStaffByCountry(countryRepo.findById(countryId).get()).stream().peek(ob -> {
			ob.setCountryId(ob.getCountry().getId());
			ob.setCountryName(ob.getCountry().getName());
		}).collect(Collectors.toList());
	}

	/*************************************************************************
	 * Update {@link CoachingStaff}
	 * 
	 * @param ob {@link CoachingStaff} object
	 * @return {@link CoachingStaff}
	 *************************************************************************/

	public CoachingStaff update(CoachingStaff ob, HttpServletResponse rs) {
		try {
			ob.setCountry(countryRepo.findById(ob.getCountryId()).orElse(null));
			return repo.save(ob);
		} catch (Exception e) {
			rs.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return ob;
		}
	}

	/*************************************************************************
	 * Delete {@link CoachingStaff}
	 * 
	 * @param id Id of CoachingStaff
	 * @return
	 *************************************************************************/

	public String delete(Long id) {
		repo.deleteById(id);
		return "Deleted Sucessfully!!!!!!!";
	}

}
